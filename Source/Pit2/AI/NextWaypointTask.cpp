#include "NextWaypointTask.h"
#include "AIController.h"
#include "Interfaces/PatrolObject.h"
#include "Engine/TargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UNextWaypointTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) {
	
	AAIController *aiController = Cast<AAIController>(OwnerComp.GetOwner());
	if (!aiController)
		return EBTNodeResult::Type::Failed;
	
	
	IPatrolObject* patrolObject = Cast<IPatrolObject>(aiController->GetPawn());
	if (!patrolObject)
		return EBTNodeResult::Type::Failed;

	TArray<ATargetPoint*> waypoints = patrolObject->GetWaypoints();
	FNextWaypointData* data = (FNextWaypointData*)NodeMemory;
	data->currentIndex++;

	if (data->currentIndex < 0 || data->currentIndex >= waypoints.Num())
		data->currentIndex = 0;	

	ATargetPoint* currentWaypoint = waypoints[data->currentIndex];
	UBlackboardComponent* blackboard = OwnerComp.GetBlackboardComponent();
	blackboard->SetValueAsVector(bbTarget.SelectedKeyName, currentWaypoint->GetActorLocation());

	return EBTNodeResult::Type::Succeeded;
}

uint16 UNextWaypointTask::GetInstanceMemorySize() const {
	return sizeof(FNextWaypointData);
}