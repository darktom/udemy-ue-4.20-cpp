// Fill out your copyright notice in the Description page of Project Settings.

#include "AttackTask.h"
#include "Characters/Enemy.h"
#include "GameFramework/Actor.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UAttackTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) {
	//GetWorld()->SpawnActor<AActor>(bulletToSpawn.Get(), OwnerComp.GetOwner()->GetActorLocation(), OwnerComp.GetOwner()->GetActorRotation());
	UBlackboardComponent *blackboard = OwnerComp.GetBlackboardComponent();
	UObject* targetObject = blackboard->GetValueAsObject(bbSelfActor.SelectedKeyName);
	AActor* targetActor = Cast<AActor>(targetObject);

	if (targetActor) {
		AEnemy* enemy = Cast<AEnemy>(targetActor);
		if (enemy)
			enemy->OnAttack();
		else
			UE_LOG(LogTemp, Warning, TEXT("(UAttackTask::ExecuteTask) NO ENEMY"));
	}
	return EBTNodeResult::Type::Succeeded;
}


