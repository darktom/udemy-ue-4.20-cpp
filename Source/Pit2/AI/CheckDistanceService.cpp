#include "CheckDistanceService.h"
#include "BehaviorTree/BlackboardComponent.h"

void UCheckDistanceService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) {
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);


	UBlackboardComponent *blackboard = OwnerComp.GetBlackboardComponent();
	UObject* targetObject = blackboard->GetValueAsObject(bbTarget.SelectedKeyName);
	AActor* targetActor = Cast<AActor>(targetObject);

	if (targetActor) {
		FVector controlledPawnLocation = OwnerComp.GetOwner()->GetActorLocation();
		FVector targetActorLocation = targetActor->GetActorLocation();
		FVector diff = controlledPawnLocation - targetActorLocation;
		float dist = diff.Size();

		if (dist < minDistance)
			blackboard->SetValueAsBool(bbCheck.SelectedKeyName, true);
		else
			blackboard->SetValueAsBool(bbCheck.SelectedKeyName, false);
	}
}
