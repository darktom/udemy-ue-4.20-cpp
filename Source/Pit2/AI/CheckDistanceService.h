#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "CheckDistanceService.generated.h"

UCLASS()
class PIT2_API UCheckDistanceService : public UBTService_BlackboardBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere) float minDistance = 1;
	UPROPERTY(EditAnywhere) FBlackboardKeySelector bbTarget;
	UPROPERTY(EditAnywhere) FBlackboardKeySelector bbCheck;

	void TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) override;

};
