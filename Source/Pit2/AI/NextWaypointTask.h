#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "NextWaypointTask.generated.h"

struct FNextWaypointData {
	int currentIndex = -1;
};

UCLASS()
class PIT2_API UNextWaypointTask : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere) FBlackboardKeySelector bbTarget;

	uint16 GetInstanceMemorySize() const override;

protected:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) override;
	
};
