#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Pit2GameModeBase.generated.h"

UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUknown
};

UCLASS()
class PIT2_API APit2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	class APlayerPawn *player;

	APit2GameModeBase();
	
	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;
	
	UFUNCTION(BlueprintPure, Category = "Health") EGamePlayState GetCurrentState() const;
	void SetCurrentState(EGamePlayState newState);

private:
	EGamePlayState currentState;

	void HandleNewState(EGamePlayState newState);
};
