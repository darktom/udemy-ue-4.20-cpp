#include "Pit2GameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "HUDs/InGameHUD.h"
#include "Characters/PlayerPawn.h"
#include "Components/HealthComponent.h"

APit2GameModeBase::APit2GameModeBase() {
	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/RealTimeElements/PlayerPawn_BP"));
	if (PlayerPawnClassFinder.Class != nullptr) {
		DefaultPawnClass = PlayerPawnClassFinder.Class;
	}
	else 
		UE_LOG(LogTemp, Warning, TEXT("(APit2GameModeBase::APit2GameModeBase) NO PlayerPawnClassFinder"));

	// use our custom HUD class
	HUDClass = AInGameHUD::StaticClass();
}

void APit2GameModeBase::BeginPlay() {
	Super::BeginPlay();

	SetCurrentState(EGamePlayState::EPlaying);
	player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (!player) {
		UE_LOG(LogTemp, Warning, TEXT("(APit2GameModeBase::BeginPlay) NO player"));
	}
}

void APit2GameModeBase::Tick(float deltaTime) {
	Super::Tick(deltaTime);

	if (player) {
		if (FMath::IsNearlyZero(player->lifeComponent->GetHealth(), 0.001f)) {
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}

EGamePlayState APit2GameModeBase::GetCurrentState() const {
	return currentState;
}

void APit2GameModeBase::HandleNewState(EGamePlayState newState) {
	switch (newState) {
	case EGamePlayState::EPlaying:
		// Do nothing
		break;

	case EGamePlayState::EGameOver:
		// Show GameOver Screen
		//UE_LOG(LogTemp, Warning, TEXT("(APit2GameModeBase::HandleNewState) GAME OVER. Restart World."));
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Player dead");
		//GetWorld()->ServerTravel("/Maps/World");
		break;
	case EGamePlayState::EUknown:
	default:
		// Do nothing 
		break;
	}
}

void APit2GameModeBase::SetCurrentState(EGamePlayState newState) {
	currentState = newState;
	HandleNewState(currentState);
}

