#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "InGameHUD.generated.h"

UCLASS()
class PIT2_API AInGameHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	AInGameHUD();
	virtual void DrawHUD() override;
	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere, Category = "Health") TSubclassOf<class UUserWidget> hudWidgetClass;
	UPROPERTY(EditAnywhere, Category = "Health") class UUserWidget *currentWidget;
};
