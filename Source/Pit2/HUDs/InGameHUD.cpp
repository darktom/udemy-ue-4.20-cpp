#include "InGameHUD.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

AInGameHUD::AInGameHUD(){
	static ConstructorHelpers::FClassFinder<UUserWidget> healthBarObj(TEXT("/Game/Blueprints/RealTimeElements/UI/RealTime_UI"));
	hudWidgetClass = healthBarObj.Class;
}

void AInGameHUD::DrawHUD() {
	Super::DrawHUD();
}

void AInGameHUD::BeginPlay(){
	Super::BeginPlay();

	if (hudWidgetClass != nullptr) {
		currentWidget = CreateWidget<UUserWidget>(GetWorld(), hudWidgetClass);

		if (currentWidget) {
			currentWidget->AddToViewport();
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("(AInGameHUD::BeginPlay) NO currentWidget"));
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("(AInGameHUD::BeginPlay) NO hudWidgetClass"));
	}
}
