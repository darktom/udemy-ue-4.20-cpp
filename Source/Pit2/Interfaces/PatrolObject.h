#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/TargetPoint.h"
#include "PatrolObject.generated.h"

UINTERFACE(MinimalAPI)
class UPatrolObject : public UInterface 
{
	GENERATED_BODY()
};

class PIT2_API IPatrolObject 
{
	GENERATED_BODY()

public:
	
	virtual TArray<ATargetPoint*> GetWaypoints() = 0;
};
