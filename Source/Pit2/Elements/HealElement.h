#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HealElement.generated.h"

UCLASS()
class PIT2_API AHealElement : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere) float amountHeal;

	AHealElement();
	UFUNCTION()	void OnOverlap(AActor* myActor, AActor* otherActor);
};
