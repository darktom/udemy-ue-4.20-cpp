#include "Elements/HealElement.h"
#include "Characters/PlayerPawn.h"
#include "Components/HealthComponent.h"

AHealElement::AHealElement() {
	amountHeal = 100.0f;

	OnActorBeginOverlap.AddDynamic(this, &AHealElement::OnOverlap);
}


void AHealElement::OnOverlap(AActor* myActor, AActor* otherActor) {
	if (otherActor != nullptr && otherActor != this) {
		APlayerPawn* player = Cast<APlayerPawn>(otherActor);

		if (player && player->lifeComponent->GetHealth() < 1.0f) {
			player->lifeComponent->UpdateHealth(amountHeal);
			Destroy();
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("(AHealElement::OnOverlap) NO BEGINOVERLAP"));
	}
}
