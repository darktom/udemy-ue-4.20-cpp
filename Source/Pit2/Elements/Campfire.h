#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/BoxComponent.h"
#include "Campfire.generated.h"

UCLASS()
class PIT2_API ACampfire : public AActor {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere) UParticleSystemComponent* fire;
	UPROPERTY(EditAnywhere) UBoxComponent* boxComponent;
	UPROPERTY(VisibleAnywhere) AActor* player;
	UPROPERTY(EditAnywhere) FHitResult hitResult;
	UPROPERTY(EditAnywhere) TSubclassOf<UDamageType> fireDamageType;
	UPROPERTY(EditAnywhere) float amountDamage;

	bool bCanApplyDamage;
	FTimerHandle fireTimerHandle;

	ACampfire();

	UFUNCTION()	void ApplyFireDamage();
	UFUNCTION()	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp,
		class AActor* OtherActor, class UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp,
		class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
