#include "Campfire.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

ACampfire::ACampfire() {
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("My Box Component"));
	boxComponent->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));
	RootComponent = boxComponent;

	fire = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("My fire"));
	fire->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	fire->SetupAttachment(RootComponent);

	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACampfire::OnOverlapBegin);
	boxComponent->OnComponentEndOverlap.AddDynamic(this, &ACampfire::OnOverlapEnd);

	bCanApplyDamage = false;
	amountDamage = 200.0f;
}

void ACampfire::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, 
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){

	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
		bCanApplyDamage = true;
		player = Cast<AActor>(OtherActor);
		//if(!player)	UE_LOG(LogTemp, Warning, TEXT("(ACampfire::OnOverlapBegin) NO player"));
		hitResult = SweepResult;
		GetWorldTimerManager().SetTimer(fireTimerHandle, this, &ACampfire::ApplyFireDamage, 2.2f, true, 0.0f);
		//UE_LOG(LogTemp, Warning, TEXT("(ACampfire::OnOverlapBegin) BEGINOVERLAP"));
	}
}

void ACampfire::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, 
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex){
	bCanApplyDamage = false;
	GetWorldTimerManager().ClearTimer(fireTimerHandle);
}

void ACampfire::ApplyFireDamage(){
	// if (!player) UE_LOG(LogTemp, Warning, TEXT("(ACampfire::ApplyFireDamage) NO player"));
	
	if (bCanApplyDamage) {
		UGameplayStatics::ApplyPointDamage(player, amountDamage, GetActorLocation(), hitResult, nullptr, this, fireDamageType);
		//UE_LOG(LogTemp, Warning, TEXT("(ACampfire::ApplyFireDamage) Applying damage"));
	}
}
