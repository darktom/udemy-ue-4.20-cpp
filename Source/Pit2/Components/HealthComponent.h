#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChangeDelegate, float, healthChange); // Event to call when health changes
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthDieDelegate); // Event to call when die


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PIT2_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true")) float fullHealth; // Max amount of health
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true")) float health; // Actual amount of health
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true")) float healthPercentage; // health / full health
	UPROPERTY(BlueprintAssignable) FHealthChangeDelegate onHealthChange; //We declare the new member for the delegate
	UPROPERTY(BlueprintAssignable) FHealthDieDelegate onDie; //We declare the new member for the delegate

	UFUNCTION(BlueprintPure) float GetHealth(); // Returns the health percentage 
	UFUNCTION(BlueprintPure) FText GetHealthIntText(); // Returns the health in a string format to use un UI
	UFUNCTION(BlueprintCallable) void UpdateHealth(float healthChange); // To change the value of the health

protected:
	virtual void BeginPlay() override;
};
