#include "MagicComponent.h"
#include "Curves/CurveFloat.h"
#include "Kismet/KismetMathLibrary.h"

UMagicComponent::UMagicComponent() {
	PrimaryComponentTick.bCanEverTick = true;
}

void UMagicComponent::BeginPlay() {
	Super::BeginPlay();

	fullMagic = 100.0f;
	magic = fullMagic;
	magicPercentage = 1.0f;
	previousMagic = magicPercentage;
	magicValue = 0.0f;
	magicPerAttack = 20;
	bCanUseMagic = true;

	if (magicCurve) {
		timelineCallback.BindUFunction(this, FName("SetMagicValue"));
		timelineFinishedCallback.BindUFunction(this, FName("SetMagicState"));
		magicTimeline.AddInterpFloat(magicCurve, timelineCallback);
		magicTimeline.SetTimelineFinishedFunc(timelineFinishedCallback);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("(APlayerPawn::BeginPlay) NO magicCurve"));
	}
}

void UMagicComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	magicTimeline.TickTimeline(DeltaTime);
}

float UMagicComponent::GetMagic() {
	return magicPercentage;
}

FText UMagicComponent::GetMagicIntText() {
	int32 mp = FMath::RoundHalfFromZero(magicPercentage * 100);
	FString mps = FString::FromInt(mp);
	FString fullMPS = FString::FromInt(fullMagic);
	FString magicHUD = mps + FString(TEXT("/") + fullMPS);
	FText mpText = FText::FromString(magicHUD);
	return mpText;
}

void UMagicComponent::SetMagicValue() {
	timelineValue = magicTimeline.GetPlaybackPosition();
	curveFloatValue = previousMagic + magicValue * magicCurve->GetFloatValue(timelineValue);
	magic = curveFloatValue * fullMagic;
	magic = FMath::Clamp(magic, 0.0f, fullMagic);
	magicPercentage = curveFloatValue;
	magicPercentage = FMath::Clamp(magicPercentage, 0.0f, 1.0f);
}

void UMagicComponent::SetMagicState() {
	bCanUseMagic = true;
	magicValue = 0.0f;
}

void UMagicComponent::UpdateMagic() {
	previousMagic = magicPercentage;
	magicPercentage = magic / fullMagic;
	magicValue = 1.0f;
	magicTimeline.PlayFromStart();
}

void UMagicComponent::SetMagicChange(float magicChange) {
	bCanUseMagic = false;
	previousMagic = magicPercentage;
	magicValue = magicChange / fullMagic;
	magicTimeline.PlayFromStart();
}

