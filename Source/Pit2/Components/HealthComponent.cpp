#include "HealthComponent.h"

void UHealthComponent::BeginPlay() {
	Super::BeginPlay();

	fullHealth = 1000.0f;
	health = fullHealth;
	healthPercentage = 1.0f;
}

// Returns the health percentage
float UHealthComponent::GetHealth() {
	return healthPercentage;
}

void UHealthComponent::UpdateHealth(float healthChange) {
	health += healthChange;
	health = FMath::Clamp(health, 0.0f, fullHealth);
	healthPercentage = health / fullHealth;
	
	onHealthChange.Broadcast(healthChange);

	if (health <= 0)
		onDie.Broadcast();
}

FText UHealthComponent::GetHealthIntText() {
	int32 hp = FMath::RoundHalfFromZero(healthPercentage * 100);
	FString hps = FString::FromInt(hp);
	FString healthHUD = hps + FString(TEXT("%"));
	FText HPText = FText::FromString(healthHUD);
	return HPText;
}