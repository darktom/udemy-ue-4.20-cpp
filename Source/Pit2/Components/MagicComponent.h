
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/TimelineComponent.h"
#include "MagicComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PIT2_API UMagicComponent : public UActorComponent 
{
	GENERATED_BODY()

public:	
	UMagicComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic") float magic; // Actual magic points
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic") float fullMagic; // Max amount of magic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic") float magicPercentage; // magic / full magic
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Magic") float previousMagic; // To smooth decreasing magic
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Magic") float magicValue;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Magic") float magicPerAttack; // Amount of magic use when attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic") UCurveFloat *magicCurve;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Magic") bool bCanUseMagic;
	UPROPERTY(VisibleAnywhere, Category = "Magic") FTimeline magicTimeline;
	FOnTimelineFloat timelineCallback; // Runs to reduce magic value
	FOnTimelineEventStatic timelineFinishedCallback; // Stops que reduction magic value
	float curveFloatValue;
	float timelineValue;
	
	UFUNCTION(BlueprintPure, Category = "Magic") float GetMagic();
	UFUNCTION(BlueprintPure, Category = "Magic") FText GetMagicIntText();
	UFUNCTION() void UpdateMagic();
	UFUNCTION() void SetMagicValue();
	UFUNCTION() void SetMagicState();
	UFUNCTION() void SetMagicChange(float value);

};
