 #include "PlayerPawn.h"

#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/HealthComponent.h"
#include "Components/MagicComponent.h"
#include "Components/TimelineComponent.h"

#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"

#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Kismet/GameplayStatics.h"



APlayerPawn::APlayerPawn() {
	PrimaryActorTick.bCanEverTick = true;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &APlayerPawn::OnOverlapBegin);


	// create the weapon static mesh component
	weaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon Mesh"));
	if (weaponMesh != nullptr) {
		weaponMesh->SetupAttachment(GetMesh(), TEXT("WeaponSocket"));
		weaponMesh->OnComponentBeginOverlap.AddDynamic(this, &APlayerPawn::OnOverlapBegin);
	}

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	stimuliSource = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("Stimuli source"));
	if (stimuliSource != nullptr) {
		stimuliSource->bAutoRegister = true;
	}

	lifeComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Life Component"));
	staminaComponent = CreateDefaultSubobject<UMagicComponent>(TEXT("Stamina Component"));

}

void APlayerPawn::BeginPlay() {
	Super::BeginPlay();

	bCanBeDamaged = true;
	isAttacking = false;
}

#pragma region Input

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("VerticalAxisMovement", this, &APlayerPawn::VerticalAxisMovement);
	InputComponent->BindAxis("HorizontalAxisMovement", this, &APlayerPawn::HorizontalAxisMovement);

	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	InputComponent->BindAction("Attack", IE_Pressed, this, &APlayerPawn::OnAttack);
}

void APlayerPawn::VerticalAxisMovement(float value) {
	if (value != 0.0f && !isAttacking) {
		const FRotator YawOnlyRotation = FRotator(0.0f, GetControlRotation().Yaw, 0.0f);
        AddMovementInput(FRotationMatrix(YawOnlyRotation).GetUnitAxis(EAxis::X), value);
	}
}

void APlayerPawn::HorizontalAxisMovement(float value) {
	if (value != 0.0f && !isAttacking) {
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, value);
	}
}

#pragma endregion

#pragma region Attack

void APlayerPawn::OnAttack() {
	if (!FMath::IsNearlyZero(staminaComponent->magic, staminaComponent->magicPerAttack)) {
		if (isAttacking)
			saveAttack = true;
		else {
			if (weaponOverheatMaterial)
				weaponMesh->SetMaterial(0, weaponOverheatMaterial);

			isAttacking = true;
			
			// Update the combo movement
			OnComboCounterChange();
			
			comboCounter++;
			if (comboCounter >= comboMaxCounter)
				comboCounter = 0;
		}

		staminaComponent->magicTimeline.Stop();
		GetWorldTimerManager().ClearTimer(magicTimerHandle);
		staminaComponent->SetMagicChange(-1 * staminaComponent->magicPerAttack);
		GetWorldTimerManager().SetTimer(magicTimerHandle, this, &APlayerPawn::OnMagicChange, 3.0f, false);
	}
}

void APlayerPawn::OnMagicChange() {
	staminaComponent->UpdateMagic();
}

void APlayerPawn::ResetCombo_Implementation() {
	comboCounter = 0;
	saveAttack = false;
	isAttacking = false;

	if (weaponDefaultMaterial)
		weaponMesh->SetMaterial(0, weaponDefaultMaterial);
}

void APlayerPawn::SaveComboAttack_Implementation() {
	if (saveAttack) {
		saveAttack = false;

		// Update the combo movement
		OnComboCounterChange();
		
		comboCounter++;
		if (comboCounter >= comboMaxCounter)
			comboCounter = 0;
	}
}

#pragma endregion

#pragma region Damage

void APlayerPawn::SetDamageState() {
	bCanBeDamaged = true; // The player can be damaged
}

void APlayerPawn::DamageTimer() {
	GetWorldTimerManager().SetTimer(damageTimerHandle, this, &APlayerPawn::SetDamageState, 2.0f, false);
}

bool APlayerPawn::PlayFlash() {
	if (redFlash) {
		redFlash = false; // Reser the flag
		return true; // Play the flash animation
	}

	return false; // Already playing the animation 
}

float APlayerPawn::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, 
	class AController * EventInstigator, AActor * DamageCauser) {
	if (bCanBeDamaged) {
		bCanBeDamaged = false;
		redFlash = true;
		lifeComponent->UpdateHealth(-DamageAmount);
		DamageTimer();
		if (lifeComponent->GetHealth() <= 0.0f) {
			OnPlayerDie();
		}
		else {
			if (!isAttacking) // Avoid to bug while attacking
				OnGetDamage();
		}
		return DamageAmount;
	}
	return 0;
}

void APlayerPawn::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {

	if (isAttacking) {
		if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
			AActor* enemy = Cast<AActor>(OtherActor);
			//if (!enemy) UE_LOG(LogTemp, Warning, TEXT("(APlayerPawn::OnOverlapBegin) NO enemy"));

			UGameplayStatics::ApplyPointDamage(enemy, amountDamage, GetActorLocation(), SweepResult, nullptr, this, NULL);
		}
	}
}

void APlayerPawn::OnPlayerDie() {
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	weaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	isAttacking = false;
	isDied = true;

	DisableInput(GetWorld()->GetFirstPlayerController());
	OnDie(); // To play die animation montage from bp

#if UE_BUILD_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "APlayerPawn::OnDie_Implementation");
#endif
}


#pragma endregion