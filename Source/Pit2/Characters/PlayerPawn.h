#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerPawn.generated.h"

UCLASS()
class PIT2_API APlayerPawn : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerPawn();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true")) class UAIPerceptionStimuliSourceComponent* stimuliSource;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true")) class UStaticMeshComponent *weaponMesh;

	// ------------------- CAMERA ELEMENTS -------------------
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true")) class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true")) class UCameraComponent* FollowCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera) float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera) float BaseLookUpRate;

	// ------------------- COMBO SYSTEM ELEMENTS -------------------
	UPROPERTY(EditAnywhere, Category = "ComboSystem") class UMaterialInterface *weaponDefaultMaterial; // Default material of the weapon to restore
	UPROPERTY(EditAnywhere, Category = "ComboSystem") class UMaterialInterface *weaponOverheatMaterial; // Custom material to apply when attacks to visual delay
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ComboSystem") int comboCounter; // Modified in blueprint
	UPROPERTY(VisibleAnywhere, Category = "ComboSystem") int comboMaxCounter = 3;
	UPROPERTY(EditAnywhere, Category = "ComboSystem") bool saveAttack;
	UPROPERTY(EditAnywhere, Category = "ComboSystem") bool isAttacking;

	UFUNCTION(BlueprintImplementableEvent, Category = "ComboSystem") void OnComboCounterChange(); //Event to play animations in blueprints
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ComboSystem") void ResetCombo();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ComboSystem") void SaveComboAttack();

	// ------------------- DAMAGE ELEMENTS -------------------
	FTimerHandle damageTimerHandle; // Delay between attacks
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage") float redFlash; // When get damage
	UPROPERTY(EditAnywhere, Category = "Damage") float amountDamage = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Damage") bool isDied = false;

	UFUNCTION(BlueprintCallable, Category = "Damage") void OnPlayerDie(); 
	UFUNCTION(BlueprintImplementableEvent, Category = "Damage") void OnDie(); // To play die animation in blueprints
	UFUNCTION(BlueprintImplementableEvent, Category = "Damage") void OnGetDamage(); // To play hit animation in blueprints
	UFUNCTION(BlueprintPure, Category = "Damage") bool PlayFlash();
	UFUNCTION(Category = "Damage") void DamageTimer();
	UFUNCTION(Category = "Damage") void SetDamageState();
	UFUNCTION() void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// ------------------- STATS ELEMENTS -------------------
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Default, meta = (AllowPrivateAccess = "true")) class UHealthComponent* lifeComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Default, meta = (AllowPrivateAccess = "true")) class UMagicComponent* staminaComponent;
	UPROPERTY(VisibleAnywhere, Category = "Magic") FTimerHandle magicTimerHandle; // Full refill magic bar

	UFUNCTION(Category = "Magic") void OnMagicChange();

protected:
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser);

	void VerticalAxisMovement(float value);
	void HorizontalAxisMovement(float value);

	void OnAttack();
	
};
