#include "TurnBasePlayer.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"

ATurnBasePlayer::ATurnBasePlayer(){
	PrimaryActorTick.bCanEverTick = true;
	

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
}

void ATurnBasePlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent){
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("VerticalAxisMovement", this, &ATurnBasePlayer::VerticalAxisMovement);
	InputComponent->BindAxis("HorizontalAxisMovement", this, &ATurnBasePlayer::HorizontalAxisMovement);

	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void ATurnBasePlayer::VerticalAxisMovement(float value) {
	if (value != 0.0f) {
		const FRotator YawOnlyRotation = FRotator(0.0f, GetControlRotation().Yaw, 0.0f);
		AddMovementInput(FRotationMatrix(YawOnlyRotation).GetUnitAxis(EAxis::X), value);
	}
}

void ATurnBasePlayer::HorizontalAxisMovement(float value) {
	if (value != 0.0f) {
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, value);
	}
}