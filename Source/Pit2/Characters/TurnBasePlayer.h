// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TurnBasePlayer.generated.h"

UCLASS()
class PIT2_API ATurnBasePlayer : public ACharacter
{
	GENERATED_BODY()

public:
	ATurnBasePlayer();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	void VerticalAxisMovement(float value);
	void HorizontalAxisMovement(float value);
	
};
