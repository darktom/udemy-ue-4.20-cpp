#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/PatrolObject.h"
#include "Engine/TargetPoint.h"
#include "Enemy.generated.h"

// You would declare the following delegate.

UCLASS()
class PIT2_API AEnemy : public ACharacter, public IPatrolObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere) bool died = false;
	UPROPERTY(EditAnywhere) float life = 100;
	UPROPERTY(EditAnywhere) float amountDamage = 100;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) bool isAttacking;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true")) TArray<ATargetPoint*> waypoints;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Default, meta = (AllowPrivateAccess = "true")) class UHealthComponent* lifeComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true")) class UStaticMeshComponent* weaponMesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true")) class UWidgetComponent *floatingDamageText;

	UFUNCTION(BlueprintImplementableEvent, Category = "Damage") void OnGetDamageAnimation(); // To play hit animation in blueprints
	UFUNCTION(BlueprintImplementableEvent, Category = "Damage") void OnDieAnimation(); // To play hit animation in blueprints
	UFUNCTION(BlueprintImplementableEvent, Category = "Damage") void OnAttackAnimation(); // To play hit animation in blueprints
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ComboSystem") void OnFinishAttack();

	UFUNCTION() void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	AEnemy();
	TArray<ATargetPoint*> GetWaypoints() override;
	void OnDie();
	void OnDieCountdown();
	void OnAttack();
	
protected:
	FTimerHandle dieTimerHandler;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser);

};
