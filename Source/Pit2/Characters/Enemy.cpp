#include "Enemy.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Components/HealthComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AEnemy::AEnemy() {
	PrimaryActorTick.bCanEverTick = true;
	
	weaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	if (weaponMesh) {
		weaponMesh->SetupAttachment(GetMesh(), TEXT("WeaponSocket"));
		weaponMesh->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnOverlapBegin);
	}

	lifeComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Life Component"));
	isAttacking = false;
}

void AEnemy::OnAttack() {
	if (!isAttacking && !died) {
		isAttacking = true;
		OnAttackAnimation();
	}
}

void AEnemy::OnDie() {
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	weaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	isAttacking = false;
	died = true;
	GetMovementComponent()->Deactivate(); // Avoid to move when dies
	GetWorldTimerManager().SetTimer(dieTimerHandler, this, &AEnemy::OnDieCountdown, 2.0f, false); // Timer to destroy actor in world

	OnDieAnimation();
}

void AEnemy::OnDieCountdown() {
	isAttacking = false;
	died = true;
	Destroy();
}

void AEnemy::OnFinishAttack_Implementation() {
	isAttacking = false;
}

TArray<ATargetPoint*> AEnemy::GetWaypoints() {
	return waypoints;
}

float AEnemy::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) {
	isAttacking = false;
	lifeComponent->UpdateHealth(-DamageAmount);

	if (lifeComponent->GetHealth() <= 0)
		OnDie();
	else
		OnGetDamageAnimation();

	return DamageAmount;
}

void AEnemy::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor,
	UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {

	if (isAttacking) {
		if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
			AActor* player = Cast<AActor>(OtherActor);

			UGameplayStatics::ApplyPointDamage(player, amountDamage, GetActorLocation(), SweepResult, nullptr, this, NULL);
			//UE_LOG(LogTemp, Warning, TEXT("(AEnemy::OnOverlapBegin) BEGINOVERLAP"));
		}
	}
}