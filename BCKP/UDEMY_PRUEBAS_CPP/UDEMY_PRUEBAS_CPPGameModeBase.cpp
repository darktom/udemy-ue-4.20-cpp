// Fill out your copyright notice in the Description page of Project Settings.

#include "UDEMY_PRUEBAS_CPPGameModeBase.h"
#include "Engine/World.h"

AUDEMY_PRUEBAS_CPPGameModeBase::AUDEMY_PRUEBAS_CPPGameModeBase() {
	PrimaryActorTick.bCanEverTick = true;
}

void AUDEMY_PRUEBAS_CPPGameModeBase::Tick(float deltaSeconds) {
	if (scoreActual >= scoreToWin) {
		UE_LOG(LogTemp, Warning, TEXT("WIN"));

		GetWorld()->ServerTravel("map2");
	}
}
