#include "GameHud.h"
#include "Engine/World.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Jugador.h"
#include "UDEMY_PRUEBAS_CPPGameModeBase.h"

void UGameHud::NativeConstruct() {

	AGameModeBase* gm = GetWorld()->GetAuthGameMode();
	if (gm != nullptr) {
		gameMode = Cast<AUDEMY_PRUEBAS_CPPGameModeBase>(gm);
	}

	APawn* pawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (pawn != nullptr)
		jugador = Cast<AJugador>(pawn);
	//modo = GetWorld()->GetAuthGameMode<AUDEMY_PRUEBAS_CPPGameModeBase>();
}

void UGameHud::NativeTick(const FGeometry& myGeometry, float deltaTime) {
	//FString::SanitizeFloat
	FString textToLabel = FString::FromInt(gameMode->scoreActual);
	labelText->SetText(FText::FromString(textToLabel));
	lifeBar->SetPercent(0.1f);
}





