// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameHud.generated.h"

/**
 * 
 */
UCLASS()
class UDEMY_PRUEBAS_CPP_API UGameHud : public UUserWidget
{
	GENERATED_BODY()
	
	UPROPERTY(meta = (BindWidget)) class UProgressBar* lifeBar;
	UPROPERTY(meta = (BindWidget)) class UTextBlock* labelText;

	class AUDEMY_PRUEBAS_CPPGameModeBase* gameMode;
	class AJugador* jugador;

	void NativeConstruct() override;
	void NativeTick(const FGeometry& geometry, float deltaTime) override;
	
	
};
