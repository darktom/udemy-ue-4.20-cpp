#include "Enemy.h"


AEnemy::AEnemy() {
	PrimaryActorTick.bCanEverTick = true;
}

void AEnemy::Tick(float DeltaTime) {
	AddActorLocalOffset(FVector(0, 2, 0), true);

	if (life <= 0)
		Destroy();
}

