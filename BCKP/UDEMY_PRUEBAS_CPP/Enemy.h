#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API AEnemy : public AActor 
{
	GENERATED_BODY()
public:	
	UPROPERTY(EditAnywhere)
	float life;

	AEnemy(); // Sets default values for this actor's properties
	virtual void Tick(float DeltaTime) override; // Called every frame
};
