#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Trap.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API ATrap : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere) 
	float damage;

	UFUNCTION()
	void OnOverlap(AActor* me, AActor* other);

protected:
	virtual void BeginPlay() override;
};
