#include "Jugador.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "UDEMY_PRUEBAS_CPPGameModeBase.h"

AJugador::AJugador() {
	PrimaryActorTick.bCanEverTick = true;
}

void AJugador::Tick(float DeltaTime) {
	if (life <= 0) {
		//Destroy();
		SetActorLocationAndRotation(initialPosition, FRotator(0, 0, 0));
		life = initialLife;
	}
}

void AJugador::SetupPlayerInputComponent(UInputComponent* inputComponent) {
	InputComponent->BindAxis("VerticalMovement", this, &AJugador::VerticalAxisMovement); // Asociacion del axis llamado vertical a la funcion VerticalAxis
	InputComponent->BindAxis("HorizontalMovement", this, &AJugador::HorizontalAxisMovement);

	InputComponent->BindAxis("VerticalCamera", this, &AJugador::VerticalAxisCamera); // Asociacion del axis llamado vertical a la funcion VerticalAxis
	InputComponent->BindAxis("HorizontalCamera", this, &AJugador::HorizontalAxisCamera);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AJugador::FireButtonPressed);
	InputComponent->BindAction("Fire", IE_Released, this, &AJugador::FireButtonReleased);
}

void AJugador::BeginPlay() {
	Super::BeginPlay();
	life = initialLife;
	initialPosition = GetActorLocation();
}

void AJugador::VerticalAxisMovement(float value) {
	float deltaTime = GetWorld()->GetDeltaSeconds();
	float movement = deltaTime * velocityInCm;

	AddActorLocalOffset(FVector(movement * value, 0, 0));
}

void AJugador::HorizontalAxisMovement(float value) {
	float deltaTime = GetWorld()->GetDeltaSeconds();
	float movement = deltaTime * velocityInCm;

	AddActorLocalOffset(FVector(0, movement * value, 0));
}

void AJugador::VerticalAxisCamera(float value) {
	float deltaTime = GetWorld()->GetDeltaSeconds();
	float rotationVelocity = deltaTime * rotationSpeed;

	AddActorLocalRotation(FRotator(-1 * value * rotationVelocity, 0, 0));
}

void AJugador::HorizontalAxisCamera(float value) {
	float deltaTime = GetWorld()->GetDeltaSeconds();
	float rotationVelocity = deltaTime * rotationSpeed;

	AddActorLocalRotation(FRotator(0, value * rotationVelocity, 0));
}

void AJugador::FireButtonPressed() {
	GetWorldTimerManager().SetTimer(fireTimer, this, &AJugador::Fire, fireRate, true);
}

void AJugador::FireButtonReleased() {
	GetWorldTimerManager().ClearTimer(fireTimer);
}

void AJugador::Fire() {
	GetWorld()->SpawnActor<AActor>(bulletToFire, GetActorLocation(), GetActorRotation());

	//AUDEMY_PRUEBAS_CPPGameModeBase
	AGameModeBase* gameMode = GetWorld()->GetAuthGameMode();
	AUDEMY_PRUEBAS_CPPGameModeBase* cppGameMode = Cast<AUDEMY_PRUEBAS_CPPGameModeBase>(gameMode);

	AUDEMY_PRUEBAS_CPPGameModeBase* gameMode2 = GetWorld()->GetAuthGameMode<AUDEMY_PRUEBAS_CPPGameModeBase>();
	gameMode2->scoreActual++;
}