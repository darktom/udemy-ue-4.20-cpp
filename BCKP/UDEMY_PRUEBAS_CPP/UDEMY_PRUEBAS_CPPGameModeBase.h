// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UDEMY_PRUEBAS_CPPGameModeBase.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API AUDEMY_PRUEBAS_CPPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere) int scoreActual;
	UPROPERTY(EditAnywhere)	int scoreToWin;

	AUDEMY_PRUEBAS_CPPGameModeBase();
	void Tick(float deltaSeconds) override;

};
