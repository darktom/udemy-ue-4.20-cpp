// Fill out your copyright notice in the Description page of Project Settings.

#include "MyUserWidget.h"
#include "Engine/World.h"
#include "GenericPlatform/GenericPlatformMisc.h"

void UMyUserWidget::Play() {
	GetWorld()->ServerTravel("map1");
}

void UMyUserWidget::Quit() {
	FGenericPlatformMisc::RequestExit(false);
}