#include "VerticalTrap.h"
#include "Engine/Public/TimerManager.h"
#include "Jugador.h"

AVerticalTrap::AVerticalTrap() {
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AVerticalTrap::BeginPlay() {
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(timerHandle, this, &AVerticalTrap::ChangeDirection, timeToChange, true);
	OnActorBeginOverlap.AddDynamic(this, &AVerticalTrap::OnOverlap);
}

// Called every frame
void AVerticalTrap::Tick(float DeltaTime) {
	FVector movement(0, 0, 0);
	movement.Z = velocity * DeltaTime;
	AddActorLocalOffset(movement, true);

}

void AVerticalTrap::ChangeDirection() {
	velocity *= -1;
}

void AVerticalTrap::OnOverlap(AActor *me, AActor *other) {
	AJugador* pj = Cast<AJugador>(other);
	if (pj != nullptr) { // Cast failed
		pj->life -= damage;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("(AVerticalTrap::OnOverlap) CAST FAILED"));
	}
}
