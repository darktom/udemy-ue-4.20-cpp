#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "game_hud.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API Ugame_hud : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget)) class UProgressBar* lifeBar;
	UPROPERTY(meta = (BindWidget)) class UTextBlock* labelText;

	class AUDEMY_PRUEBAS_CPPGameModeBase* gameMode;
	class AJugador* jugador;

	void NativeConstruct() override;
	void NativeTick(const FGeometry& geometry, float deltaTime) override;
	
};
