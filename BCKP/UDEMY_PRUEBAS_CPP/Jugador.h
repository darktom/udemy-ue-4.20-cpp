
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Jugador.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API AJugador : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere) float life;

	AJugador();
	virtual void Tick(float DeltaTime) override;
	void SetupPlayerInputComponent(class UInputComponent* inputComponent) override;


protected:

	UPROPERTY(EditAnywhere)	float initialLife;
	UPROPERTY(VisibleAnywhere)	FVector initialPosition;

	FTimerHandle fireTimer;
	UPROPERTY(EditAnywhere) TSubclassOf<class AActor> bulletToFire;
	UPROPERTY(EditAnywhere) int ammunition;

	UPROPERTY(EditAnywhere)	float fireRate;
	UPROPERTY(EditAnywhere)	float velocityInCm;
	UPROPERTY(EditAnywhere)	float rotationSpeed;

	void BeginPlay() override;

	void VerticalAxisMovement(float value);
	void HorizontalAxisMovement(float value);

	void VerticalAxisCamera(float value);
	void HorizontalAxisCamera(float value);

	void FireButtonPressed();
	void FireButtonReleased();
	void Fire();

};
