#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MyUserWidget.generated.h"


UCLASS()
class UDEMY_PRUEBAS_CPP_API UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void Play();
	
	UFUNCTION(BlueprintCallable)
	void Quit();
	
};
