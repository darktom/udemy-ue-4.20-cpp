// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VerticalTrap.generated.h"

UCLASS()
class UDEMY_PRUEBAS_CPP_API AVerticalTrap : public AActor
{
	GENERATED_BODY()

public:

	AVerticalTrap();
	virtual void Tick(float DeltaTime) override;


protected:

	UPROPERTY(EditAnywhere)
		float damage;

	UPROPERTY(EditAnywhere)
		float timeToChange;

	UPROPERTY(EditAnywhere)
		float velocity;

	FTimerHandle timerHandle;


	virtual void BeginPlay() override;
	void ChangeDirection();

	UFUNCTION()
		void OnOverlap(AActor *me, AActor *other);

};
