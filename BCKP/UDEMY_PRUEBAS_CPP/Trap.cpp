// Fill out your copyright notice in the Description page of Project Settings.

#include "Trap.h"
#include "Enemy.h"

void ATrap::OnOverlap(AActor* me, AActor* other) {
	//other->Destroy();

	AEnemy* e = Cast<AEnemy>(other);
	e->life -= damage;
}

void ATrap::BeginPlay(){
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &ATrap::OnOverlap);
}
